<h2>Synergia Api: Tutorial step by step to run this api</h2>


<h5>System dependencies</h5>

* Docker (https://www.docker.com/)
* Docker-compose

<h5>Gems</h5>

* gem 'rack-cors' for handling Cross-Origin Resource Sharing (CORS)
* gem 'rack-attack' rack middleware for blocking & throttling abusive requests
* gem 'devise_token_auth' simple, multi-client and secure token-based authentication for Rails.
* gem 'pundit' to build a simple, robust and scaleable authorization system
* gem 'brakeman' brakeman is a security scanner for Ruby on Rails applications.
* The following gems were used to help in the automated tests
  * Rspec
  * ffaker
  * factory_bot_rails

<h5>Step by step to run the project in localhost using docker-compose</h5> 

1. Download the project from wetransfer

   ```bash
   git clone https://gitlab.com/vini.freire.oliveira/synergia-api
   ```

2. Open the project folder

   ```bash
   cd synergia-api
   ```

3. Build the containers and install the dependecies

   ```dockerfile
   docker-compose run --rm app bundle install
   ```

4. Now, create database, generate migrations and run the seeds file:

   ```ruby
   docker-compose run --rm app bundle exec rails db:create db:migrate db:seed
   ```

5. Start the server with

   ```
   docker-compose up
   ```


 <h5>To run Rspec test results</h5>

1. To run the tweets service spec

   ```bash
   docker-compose run --rm app bundle exec rspec spec/requests/
   ```

2. To run all spec files

   ```bash
   docker-compose run --rm app bundle exec rspec spec
   ```

3. To run all spec files

   ```bash
   docker-compose run --rm app bundle exec rspec spec/ --format h > output_test.html
   ```

<h5>To run Breakman report results</h5>

1. To generate the report

   ```dockerfile
   docker-compose run --rm app bundle exec brakeman -f html >> report.html
   ```

   

 <h3>Links</h3>

 <h5><a href="https://documenter.getpostman.com/view/110504/S1TU3dnp">Requests document</a></h5>

 <h5><a href="https://gitlab.com/vini.freire.oliveira">My Git</a></h5>

 <h5><a href="https://www.linkedin.com/in/vinicius-freire-b53507107/">My LikedIn</a></h5>

 <h5>The <a href="<https://rubygems.org/gems/l6_http>">l6_http</a> is a gem to make http and https request developed by me. Check on <a href="<https://gitlab.com/vini.freire.oliveira/l6_http>"> GIT</a>.</h5>


 <h5>Extra docker-compose commands</h5>

1. Start the container in shared folder

   ```
   docker-compose run --rm app bash
   ```

2. Start the ruby container in ruby console

   ```
   docker-compose run --rm app bundle exec rails c
   ```
