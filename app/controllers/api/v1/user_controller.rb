class Api::V1::UserController < Api::V1::ApiController
  include ActionController::Serialization
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_users, only: [:index]
  
  def index
    render json: @users
  end

  def show
    render json: current_user
  end

  def update
    if current_user.update(user_params)
      render json: current_user
    else
      render json: current_user.errors, status: :unprocessable_entity
    end
  end

  private

  def set_users
    @users = User.where(user_type: :employee).all.order("created_at DESC")
  end

  def user_params
    params.require(:user).permit(:avatar, :curriculum, :birthdate, :marital_status, :academic, :wage_claim, phones_attributes: [:type_phone, :area_code, :number], addresses_attributes: [:cep, :street, :number, :district, :city, :state, :neighborhood, :complement])
  end

  def authorize_user
    authorize User
  end

end
