module Api::V1
    class ApiController < ApplicationController
        rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
        include Pundit

        private

        def user_not_authorized
            render json: { message: "pudit: user not authorized" }.to_json, status: :unauthorized
        end

    end
end
