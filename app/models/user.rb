# frozen_string_literal: true

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_one_attached :avatar
  has_one_attached :curriculum

  has_many :addresses, dependent: :destroy
  has_many :phones, dependent: :destroy

  accepts_nested_attributes_for :addresses, :phones

  validates :email, presence: true, uniqueness: true

  enum user_type: [:admin, :employee]
  enum marital_status: [:single, :married, :widower]
  enum academic: [:medium, :postgraduate, :graduate]

  before_validation :set_user, on: :create

  private

  def set_user
    self.user_type = :employee if User.all.count > 0
  end

end
