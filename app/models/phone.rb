class Phone < ApplicationRecord
    belongs_to :user
    
    enum type_phone: [:phone, :cellphone, :fax]
end
