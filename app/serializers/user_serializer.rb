class UserSerializer < ActiveModel::Serializer
  attributes :name, :email, :birthdate, :marital_status, :academic, :wage_claim, :phones, :addresses, :avatar, :curriculum, :user_type

  def avatar
    Rails.application.routes.url_helpers.rails_blob_path(object.avatar, only_path: true) if object.avatar.attached?
  end

  def curriculum
    Rails.application.routes.url_helpers.rails_blob_path(object.curriculum, only_path: true) if object.curriculum.attached?
  end
end
