class UserPolicy < ApplicationPolicy

  def index?
    user.admin?
  end

  def show?
    user.employee?
  end

  def update?
    user.employee?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
