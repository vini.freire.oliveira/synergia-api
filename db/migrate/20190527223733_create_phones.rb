class CreatePhones < ActiveRecord::Migration[5.2]
  def change
    create_table :phones do |t|
      t.references :user, foreign_key: true
      t.integer :type_phone
      t.string :area_code
      t.string :number
      t.timestamps
    end
  end
end
