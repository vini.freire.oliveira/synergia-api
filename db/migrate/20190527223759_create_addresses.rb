class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.references :user, foreign_key: true
      t.string :cep
      t.string :street
      t.string :number
      t.string :district
      t.string :city
      t.string :state
      t.string :neighborhood
      t.string :complement
      t.timestamps
    end
  end
end
