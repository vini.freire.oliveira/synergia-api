require 'rails_helper'

RSpec.describe UserPolicy, type: :policy do
  before(:each) do
    @admin = FactoryBot.create(:user)
    @admin.user_type = :admin
    @employee = FactoryBot.create(:user)
  end

  let(:user) { FactoryBot.create(:user) }
  let(:employee) { UserPolicy.new(@employee, user) }
  let(:admin) { UserPolicy.new(@admin, user) }

  permissions :index? do
    context "User show all curriculums" do
      it "denies access if user is an employee" do
        expect(employee).not_to permitted_to(:index?)
      end
  
      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:index?)
      end
    end
  end

  permissions :show? do
    context "User show his curriculum" do
      it "grants access if user is an employee" do
        expect(employee).to permitted_to(:show?)
      end

      it "denies access if user is an manager" do
        expect(admin).not_to permitted_to(:show?)
      end

    end
  end

  permissions :update? do
    context "User update his curriculum" do
      it "grants access if user is an employee" do
        expect(employee).to permitted_to(:update?)
      end

      it "denies access if user is an manager" do
        expect(admin).not_to permitted_to(:update?)
      end
    end
    
  end
end
