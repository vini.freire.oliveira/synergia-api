FactoryBot.define do
    factory :address do
        user
        cep { FFaker::AddressBR.unique.zip_code }
        street { FFaker::AddressBR.unique.street }
        district { FFaker::AddressBR.unique.neighborhood }
        number { FFaker::AddressBR.unique.building_number }
        state { FFaker::AddressBR.unique.state }
        city { FFaker::AddressBR.unique.city }
        neighborhood { FFaker::AddressBR.unique.neighborhood }
        complement { FFaker::AddressBR.unique.secondary_address } 
    end
end