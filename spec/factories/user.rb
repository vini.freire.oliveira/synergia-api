FactoryBot.define do
    factory :user do
        name    { FFaker::NameBR.name }
        email   { FFaker::Internet.unique.safe_email}
        password    { 'secret123' }
        password_confirmation { 'secret123' }
        birthdate   { FFaker::Time.between(Date.new(1980, 02, 01), Date.new(2010, 02, 01)) }
        wage_claim  { rand(1100...7600) }
        marital_status { "single" }
        academic { "graduate" }
    end
end
