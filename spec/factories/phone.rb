FactoryBot.define do
    factory :phone do
        type_phone { "Celular" }
        area_code { "85" }
        number { FFaker::PhoneNumberBR.mobile_phone_number }
        user
    end
end