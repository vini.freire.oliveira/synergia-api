require 'rails_helper'
 
RSpec.describe "Api::V1::User", type: :request do

  describe "GET /users" do
    context "With Invalid authentication headers" do
      it_behaves_like :deny_without_authorization, :get, "/api/v1/user"
    end
 
    context "With Valid authentication headers and without authorization" do
      before do
        @user = create(:user)
        get "/api/v1/user", params: {}, headers: header_with_authentication(@user)
      end
 
      it "returns 401" do
        expect_status(401)
      end
    end

    context "With Valid authentication headers and authorization" do
      before do
        @user = create(:user)
        @user.user_type = :admin
        get "/api/v1/user", params: {}, headers: header_with_authentication(@user)
      end
 
      it "returns 200" do
        expect_status(200)
      end

      it "returned user in the Hash format " do
        expect(json.is_a? Array).to eql(true)
      end
 
      it "returns list with all user" do
        expect(json.count).to eql(User.where(user_type: :employee).all.count)
      end
    end

  end
 
  describe "GET /user/id" do

    context "With Invalid authentication headers" do
      it_behaves_like :deny_without_authorization, :get, "/api/v1/user/1"
    end
  
    context "With Valid authentication headers and without authorization" do
      before do
        @user = create(:user)
        @user.user_type = :admin
        get "/api/v1/user/#{@user.id}", params: {}, headers: header_with_authentication(@user)
      end
 
      it "returns 401" do
        expect_status(401)
      end
    end

    context "With Valid authentication headers and authorization" do
      before do
        @user = create(:user)
        @user.user_type = :employee
        get "/api/v1/user/#{@user.id}", params: {}, headers: header_with_authentication(@user)
      end
 
      it "returns 200" do
        expect_status(200)
      end
 
      it "returns itself" do
        expect(json["email"]).to eql(@user.email)
      end
    end
    
  end
 
  describe "PUT /user/id" do

    context "With Invalid authentication headers" do
      it_behaves_like :deny_without_authorization, :put, "/api/v1/user/1"
    end
 
    context "With valid authentication headers and without authorization" do
      before do
        @user = create(:user)
        @user.user_type = :admin
        @user_attributes = attributes_for(:user)
        put "/api/v1/user/#{@user.id}", params: {user: @user_attributes}, headers: header_with_authentication(@user)
      end

      it "returns 401" do
        expect_status(401)
      end
    end

    context "With Valid authentication headers and authorization" do
      before do
        @user = create(:user)
        @user.user_type = :employee
        @user_attributes = attributes_for(:user)
        put "/api/v1/user/#{@user.id}", params: {user: @user_attributes}, headers: header_with_authentication(@user)
      end
 
      it "returns 200" do
        expect_status(200)
      end

      it "user are updated with correct data" do
        @user.reload
        expect(@user.birthdate.strftime("%d/%m/%Y")).to eql(@user_attributes[:birthdate].strftime("%d/%m/%Y"))
        expect(@user.wage_claim).to eql(@user_attributes[:wage_claim].to_f)
        expect(@user.marital_status).to eql(@user_attributes[:marital_status])
        expect(@user.academic).to eql(@user_attributes[:academic])
      end
 
      it "returns his user" do
        expect(json["email"]).to eql(@user.email)
      end
    end
  end
 
end